/* 
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * */
requires xdc.services.spec;
requires xdc.shelf;

/*!
 *  ======== xdc.services.spec.views ========
 *
 * @_nodoc
 *
 * Java classes for user presentation of spec'd declarations
 */
package xdc.services.spec.views [1, 0, 0] {
}
/*
 *  @(#) xdc.services.spec.views; 1, 0, 0,0; 11-8-2017 17:13:11; /db/ztree/library/trees/xdc/xdc-D20.1/src/packages/
 */

