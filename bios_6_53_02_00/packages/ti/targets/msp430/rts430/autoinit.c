/*
 *  Copyright 2017 by Texas Instruments Incorporated.
 *
 */

#if __TI_COMPILER_VERSION__ >= 4004000
#include "autoinit_4.4.c"
#elif __TI_COMPILER_VERSION__ >= 4002000
#include "autoinit_4.2.c"
#else
#include "autoinit_2.1.c"
#endif
/*
 *  @(#) ti.targets.msp430.rts430; 1, 0, 0,0; 11-8-2017 18:01:40; /db/ztree/library/trees/xdctargets/xdctargets-p04/src/ xlibrary

 */

